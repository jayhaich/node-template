#!/bin/sh -x

DBPATH=/tmp/node_test_dbpath
# Clean up the temporary MongoDB dir from the last run
rm -rf $DBPATH
sleep 1.0
mkdir $DBPATH

# Start a temporary MongoDB instance
mongod --port 9900 --dbpath "$DBPATH" 1>/dev/null &
pid=$!
sleep 2.0

# Run the tests
npm run test

# Stop MongoDB
kill $pid
