import cluster = require('cluster');
import express = require("express");
import promBundle = require("express-prom-bundle");
import mongoose = require('mongoose');

import cli = require("./src/cli");
import routes = require("./src/routes");
import config = require("./src/util/config");
import log = require("./src/util/log");

let args = cli.parseArgs(process.argv);
let cfg = config.load(args.config);

mongoose.connect(
  cfg.mongodb.uri,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
).then(
  () => {
    log.info(
      'MongoDBConnected',
      {
        uri: cfg.mongodb.uri
      }
    );
  }
).catch(
  (error) => {
    log.error(
      'MongoDBError',
      {
        error: error,
        uri: cfg.mongodb.uri
      }
    );
    process.exit(1);
  }
);

// Handle worker processes that crash
cluster.on('exit', function (worker) {
  log.error(
    'WorkerProcCrashedRespawn',
    {
      workerID: worker.id
    }
  );
  cluster.fork();
});

if(cluster.isMaster){
  for(let i = 0; i < args.procs; i++){
    cluster.fork();
  }
  const metricsApp = express();
  metricsApp.use('/metrics', promBundle.clusterMetrics());
  metricsApp.listen(9999);
} else {
  const app: express.Application = express();
  routes.setup(app);
  app.listen(
    args.port,
    args.host,
    () => {
      log.info(
        "WorkerProcStartListening",
        {
          listenAddress: args.host,
          port: args.port
        }
      );
    }
  );
}
