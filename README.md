# node-template
Project template for a Node.js web service

# Usage
## Forking
- Fork/clone the project
- Edit `package.json`, update with the metadata for your project
- Setup your own routes, metrics, etc...

You may wish to create a fork of this project that implements your
organization-specific logging and metric standards, and any other standards.

## Developer Workflow
```
# Compile
make

# Run
node server.js

# Test
make test
```
