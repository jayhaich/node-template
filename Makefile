all:
	npm run tsc
clean:
	find src/ -name '*.js' -delete
	rm server.js
test:
	./run_tests.sh
	# Coverage report: coverage/lcov-report/index.html
