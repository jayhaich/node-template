/*
Example model using MongoDB with Mongoose used for the schema
*/

import mongoose = require('mongoose');

export interface IFoo extends mongoose.Document {
  name: string;
  optionalField?: number;
};

export const FooSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    index: true,
  },
  optionalField: Number,
});

const Foo = mongoose.model<IFoo>('Foo', FooSchema);
export default Foo;
