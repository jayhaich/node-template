import express = require("mock-express");
import routes = require("./routes");

test('route setup', () => {
  let app = express();
  routes.setup(app);
});
