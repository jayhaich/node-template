import cli = require("./cli");

test('parseArgs', () => {
  let config = '/etc/myapp/config.json';
  let args = cli.parseArgs([
    '/usr/bin/node',
    'something.js',
    '-c',
    config,
    '--procs',
    '3',
    '-H',
    'localhost',
    '-p',
    '9090'
  ]);
  expect(args.config).toBe(config);
  expect(args.procs).toBe(3);
  expect(args.host).toBe('localhost');
  expect(args.port).toBe(9090);
});

test('parseArgs::procs==0', () => {
  let args = cli.parseArgs([
    '/usr/bin/node',
    'something.js',
    '--procs',
    '0'
  ]);
  expect(args.procs).toBe(require('os').cpus().length);
});
