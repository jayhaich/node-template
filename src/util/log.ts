import winston = require('winston');

const logger = winston.createLogger({
  format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.json()
  ),
  transports: [
    new (winston.transports.Console)()
  ]
});

export function errorHandler(error) {
  console.error('Error caught', error);
}

// Compulsory error handling
logger.on('error', errorHandler);

const pid = process.pid;
const host = require('os').hostname();

/*
Create a standard log message
@event: The name of the log event, will be used for searching and indexing
@data:  Arbitrary key/value pairs associated with this event
*/
function logMsg(
  event: string,
  data: Object
){
  return {
    data: data,
    event: event,
    host: host,
    pid: pid
  };
}

export function error(
  event: string,
  data: Object
){
  logger.error(
    logMsg(event, data)
  );
}

export function warn(
  event: string,
  data: Object
){
  logger.warn(
    logMsg(event, data)
  );
}

export function info(
  event: string,
  data: Object
){
  logger.info(
    logMsg(event, data)
  );
}

export function debug(
  event: string,
  data: Object
){
  logger.debug(
    logMsg(event, data)
  );
}

