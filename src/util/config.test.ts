import fs = require("fs");
import tmp = require("tmp");

import config = require("./config");

test('load', () => {
  let c = {
    value: 123
  };
  let j = JSON.stringify(c);
  let t = tmp.fileSync();
  fs.writeFileSync(t.name, j);
  config.load(t.name);
  let cfg = config.config()
  expect(cfg.value).toBe(123);
});
