import fs = require('fs');

interface IConfig{
  value?: string,
  mongodb: {
    db: string,
    uri: string,
  },
  mysql: {
    connectionPool: {
      connectionLimit: number,
      host: string,
      user: string,
      password: string,
      database: string,
    }
  },
}

export var TestMongoURI =
  'mongodb://127.0.0.1:9900/?retryWrites=true&w=majority';

/*
Not meant to be accessed directly, only use directly to override in
unit tests
*/
export var Config: IConfig

export function config(){
  return Config;
}

export function load(path: string): IConfig {
  let buffer = fs.readFileSync(path);
  Config = JSON.parse(buffer.toString()) as IConfig;
  return Config;
}
