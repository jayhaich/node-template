import log = require("./log");

test('errorHandler', () => {
  log.errorHandler("ERROR");
});

test('log.error', () => {
  log.error(
    "ERROR",
    {}
  );
});

test('log.warn', () => {
  log.warn(
    "WARN",
    {}
  );
});

test('log.info', () => {
  log.info(
    "INFO",
    {}
  );
});

test('log.debug', () => {
  log.debug(
    "DEBUG",
    {}
  );
});
