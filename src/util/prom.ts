/*
  This file defines all promClient metrics for this service.
  Modules that need to manipulate metrics should import this module.
  The HTTP endpoint is defined in ./routes/metrics.ts
*/

import express = require("express");
import promClient = require('prom-client');

export const metricsInterval = promClient.collectDefaultMetrics();

export const fooBarBazzes = new promClient.Counter({
  name: 'foo_bar_bazzes_total',
  help: 'Total number of foo bar bazzes',
  labelNames: [
    'foo',
    'bar',
  ],
});
