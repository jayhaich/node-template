import express = require("express");

export const path = "/health";

export function Get(req, res) {
  res.send({
    "errors": 0,
    "healthy": true
  });
}

export function setup(app: express.Application, path: string){
  app.get(path, Get);
}
