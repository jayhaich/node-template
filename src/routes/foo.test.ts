import express = require('mock-express');
import mongoose = require('mongoose');

import foo = require('./foo');
import { TestMongoURI } from '../util/config';

describe("URI /", () => {
  beforeAll(() => {
    mongoose.connect(
      TestMongoURI,
      {
        useNewUrlParser: true,
        //useUnifiedTopology: true,
      }
    );
  });

  afterAll(() => {
    mongoose.connection.close();
  });

  beforeEach(() => {
     jest.useFakeTimers();
  });

  test('Responder.respond', () => {
    let req = {};
    let res: any = {};
    res.send = res.sendStatus = (result) => {};
    res.status = (value) => { return res; };
    let r = new foo.Responder(req, res, 200, 400);
    r.respond(null, {});
  });

  test('Responder.respond error', () => {
    let req = {};
    let res: any = {};
    res.send = res.sendStatus = (result) => {};
    res.status = (value) => { return res; };
    let r = new foo.Responder(req, res, 200, 400);
    r.respond({'error': true}, {});
  });

  test('POST, GET /', () => {
    let app = express();
    let req = {
      host: 'http://www.google.com',
      body: {
        name: 'lol',
        optionalField: 123
      }
    };
    let res: any = {};
    res.send = res.sendStatus = (result) => {};
    res.status = (value) => { return res; };

    foo.Post(req, res);
    foo.Get(req, res);
  });
});
