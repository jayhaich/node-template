import express = require("express");
import promClient = require("prom-client");

export const path = "/metrics";

export function Get(req, res) {
  res.set('Content-Type', promClient.register.contentType)
  res.end(promClient.register.metrics())
}

export function setup(app: express.Application, path: string){
  app.get(path, Get);
}
