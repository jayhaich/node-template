import express = require("mock-express");
import health = require("./health");

test('GET /health', () => {
  let app = express();
  let req = app.makeRequest({ 'host': 'http://www.google.com' });
  let res = app.makeResponse(function(err, sideEffects) {
  });
  health.Get(req, res);
});
