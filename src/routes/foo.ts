import express = require("express");

import Foo from "../models/foo";
import log = require("../util/log");

export const path = "/foo";

export class Responder{
  req: express.Request;
  res: express.Response;
  status: number;
  errorStatus: number;

  constructor(
    req: express.Request,
    res: express.Response,
    status: number,
    errorStatus: number,
  ){
    this.req = req;
    this.res = res;
    this.status = status;
    this.errorStatus = errorStatus;
  }

  respond = (error: any, result: any) => {
    if(error){
      log.error('MongoDBError', { error: error });
      this.res.status(this.errorStatus).send(error);
    } else {
      this.res.status(this.status).send(result);
    }
  }

}

export function Get(req, res) {
  log.info(
    "FooHello",
    {
      message: "Hello"
    }
  );
  let r = new Responder(req, res, 200, 500);
  Foo.find(r.respond);
}

export function Post(req, res) {
  let body = req.body;
  let newFoo = new Foo(req.body);
  let r = new Responder(req, res, 200, 400);
  newFoo.save(r.respond);
}

export function setup(
  app: express.Application,
  path: string
){
  app.get(path, Get);
  app.post(path, Post);
}
