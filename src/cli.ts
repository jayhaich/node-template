import argparse = require("argparse");
import assert = require("assert");

/* Parse command line arguments
   @args:   A list containing the CLI arguments, for example, process.argv or:
            ['node', 'myscript.js', '-c', 'somefile.json']
   @return: An object containing all specified CLI arguments, for example:
            {config: 'somefile.json'}
*/
export function parseArgs(args: string[]) {
  let parser = argparse.ArgumentParser({
    addHelp: true,
    description: "TODO"
  });
  parser.addArgument(
    ['-c', '--config'],
    {
      defaultValue: "./config.json",
      dest: "config",
      help: "The path to a configuration file"
    }
  );
  parser.addArgument(
    ["-H", "--host"],
    {
      defaultValue: '0.0.0.0',
      dest: "host",
      help:
        "The hostname or IP address to listen on, " +
        "default: 0.0.0.0 (all available addresses)"
    }
  );
  parser.addArgument(
    ["-p", "--port"],
    {
      defaultValue: 8080,
      dest: "port",
      help: "The port to listen on",
      type: Number
    }
  );
  parser.addArgument(
    "--procs",
    {
      defaultValue: 1,
      dest: "procs",
      help: "The number of worker processes to run. 0 to use all CPUs",
      type: Number
    }
  );
  args = args.slice(2);
  let result = parser.parseArgs(args);
  assert(Number.isInteger(result.procs));
  assert(Number.isInteger(result.port));
  if(result.procs <= 0){
    result.procs = require('os').cpus().length;
  }
  assert(result.procs >= 1 && result.procs <= 256);
  assert(result.port >= 1 && result.port <= 65535);
  return result;
}
