import promBundle = require("express-prom-bundle");
import express = require("express");

// routes
import health = require("./routes/health");
import foo = require("./routes/foo");
import metrics = require("./routes/metrics");

export function setup(app: express.Application){
  let metricsMiddleware = promBundle({
    includeMethod: true,
    includePath: true,
    promClient: {
      collectDefaultMetrics: {}
    },
  });
  app.use(metricsMiddleware);
  foo.setup(app, foo.path);
  health.setup(app, health.path);
  metrics.setup(app, metrics.path);
}
